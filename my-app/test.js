var chai = require('chai');


var expect = chai.expect

const multiply = function(a, b){
    return a*b
}

describe('multiply function', function () {
    it('should multiply 2 number', () =>{
        const result = multiply(2,5);
        expect(result).to.equal(10)
    })
})
