const fs = require('fs');
const http = require('http');
const pug = require('pug');

const express = require('express');
const app = express();

const path = require('path');

const port = 3000;


var myArgs = process.argv[2];
if (!myArgs) {
    myArgs = "data.csv"
}

var data = fs.readFileSync('./' + myArgs,'utf8')

data = data.split('\n')
var resu = []
data.forEach(element => {
    var split = element.split(';')
    resu.push({id:split[0], ville: split[1]})
});
console.log(resu)
const compiledFunction = pug.compileFile('template.pug');

app.get('/', (req, res) => {
    const generatedTemplate = compiledFunction({
        data: resu,
    })

    res.statusCode = 200
    res.setHeader('content-type', 'text/html')
    res.send(generatedTemplate)
})

app.use(express.static(path.join(__dirname, 'image')))

app.listen(port, () => {
    console.log('serveur running at port ${port}');
})
